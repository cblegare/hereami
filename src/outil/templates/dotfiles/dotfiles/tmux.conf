###
# Use default shell
#   from https://superuser.com/a/388243
set-option -g default-shell $SHELL
# Use a proper terminal
#   from https://stackoverflow.com/questions/18600188
#   from https://github.com/tmux/tmux/wiki/FAQ#how-do-i-make-modified-function-and-arrow-keys-like-c-up-m-pageup-work-inside-tmux
set -g default-terminal tmux-256color
###

###
# Less awkward prefix keys
#   from https://www.hamvocke.com/blog/a-guide-to-customizing-your-tmux-conf/
#
# Probably the most common change among tmux users is to change the
# prefix from the rather awkward C-b to something that’s a little more
# accessible. Personally I’m using C-a instead but note that this might
# interfere with bash’s “go to beginning of line” command1. On top of
# the C-a binding I’ve also remapped my Caps Lock key to act as Ctrl
# since I’m not using Caps Lock anyways. This allows me to nicely
# trigger my prefix key combo.
#
# remap prefix from 'C-b' to 'C-a'
unbind C-b
set-option -g prefix C-a
bind-key C-a send-prefix
###

###
# reload config file (change file location to your the tmux.conf you want to use)
bind r source-file ~/.tmux.conf
#
###

###
# Vim integration
#   from https://blog.bugsnag.com/tmux-and-vim/
#
# This bit of configuration works by adding conditional logic to the
# ctrl-<direction> key bindings
# When one of these movement commands is used, it checks if the
# current tmux pane is running vim.
# If so, the appropriate vim split navigation command is sent.
# Otherwise, the appropriate tmux pane navigation command is sent.
# See: https://github.com/christoomey/vim-tmux-navigator
is_vim="ps -o state= -o comm= -t '#{pane_tty}' \
    | grep -iqE '^[^TXZ ]+ +(\\S+\\/)?g?(view|n?vim?x?)(diff)?$'"
bind-key -n C-h if-shell "$is_vim" "send-keys C-h"  "select-pane -L"
bind-key -n C-j if-shell "$is_vim" "send-keys C-j"  "select-pane -D"
bind-key -n C-k if-shell "$is_vim" "send-keys C-k"  "select-pane -U"
bind-key -n C-l if-shell "$is_vim" "send-keys C-l"  "select-pane -R"
bind-key -n 'C-\' if-shell "$is_vim" "send-keys C-\\" "select-pane -l"
bind-key -T copy-mode-vi C-h select-pane -L
bind-key -T copy-mode-vi C-j select-pane -D
bind-key -T copy-mode-vi C-k select-pane -U
bind-key -T copy-mode-vi C-l select-pane -R
bind-key -T copy-mode-vi 'C-\' select-pane -l
#
###

###
# Mouse integration
#
# Enable mouse mode (tmux 2.1 and above)
set -g mouse on
#
# Enable mouse scroll
#   see https://github.com/tmux/tmux/issues/145#issuecomment-150736967
bind -n WheelUpPane if-shell -F -t = "#{mouse_any_flag}" "send-keys -M" "if -Ft= '#{pane_in_mode}' 'send-keys -M' 'copy-mode -e'"
#
###

###
# Integrate with X clipboard
bind-key -T copy-mode-vi MouseDragEnd1Pane send -X copy-pipe "xclip -selection clipboard -i" \; send -X clear-selection
#
###

###
# Look and feel
#   from https://www.hamvocke.com/blog/a-guide-to-customizing-your-tmux-conf/
#
# loud or quiet?
set-option -g visual-activity off
set-option -g visual-bell off
set-option -g visual-silence off
set-window-option -g monitor-activity off
set-option -g bell-action none
#
# Advertise a rich terminal
set -as terminal-overrides ",*:Tc"
#
# Work around
#   "ESC in tmux or GNU Screen is delayed"
#   https://github.com/neovim/neovim/wiki/FAQ#esc-in-tmux-or-gnu-screen-is-delayed
set -sg escape-time 10
#
###
