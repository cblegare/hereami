#!/bin/sh
###
# Setup a full fledged python environment
# for this shell
PY_VERSION=3.6.7
VENV_NAME=homevenv

###
# Install pyenv
#   See https://github.com/pyenv/pyenv
#
# First, make sure the build environment is ready
echo "Installing required packages"
PKG="
make build-essential
libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev
libncurses5-dev xz-utils tk-dev libffi-dev
libxml2-dev libxmlsec1-dev
wget curl llvm
"
dpkg-query -l $PKG > /dev/null || echo $PKG | xarg sudo apt install -y

echo "installing pyenv using the pyenv-installer"
curl -L https://github.com/pyenv/pyenv-installer/raw/master/bin/pyenv-installer | bash

echo "pyenv was installed."
echo "activation might be needed, else use the oh-my-zsh plugin!"

echo "updating pyenv"
#pyenv update

echo "Setuping main shell base python"
MAIN_VERSION="$PY_VERSION"
echo "Using $MAIN_VERSION"
pyenv install --skip-existing $MAIN_VERSION
pyenv global $MAIN_VERSION
pyenv rehash
pyenv version
pip install -U pip

echo "Setuping default virtualenv for this shell"
pyenv virtualenv $VENV_NAME
pyenv global $VENV_NAME
pyenv rehash
echo $VENV_NAME > ~/.python-versions
VENV_REQS="~/.local/share/thierry/$VENV_NAME_requirements.txt"
if [ -f "$VENV_REQS" ]; then
    echo "Found requirements file for"
    pip install -U -r "$VENV_REQS"
fi

echo "Main shell python version ready!"
cat << EOF

    The python environment is all set! Specifically,

    - pyenv was installed to manage python versions,
    - python $PY_VERSION has been pyenv-installed,
    - a virtual environment named $VENV_NAME was created and
      the shell will default to it and
    - packages listed in $VENV_REQS
      where installed (if applicable),

    Please use Python for any following setup script
    because shell scripting is the friend we hate to love.
EOF

