"""
#######################
UNIX shell interpreters
#######################

Introspect and interact with the calling UNIX shell interpreter.
"""
import os
from pathlib import Path

from outil import __project__


class Shell:
    """
    Abstract interface for UNIX shell interpreters
    """

    def eval_completion(self, command: str = __project__) -> str:
        raise NotImplementedError

    def append_eval_completion(
        self, file: Path, command: str = __project__
    ) -> Path:
        raise NotImplementedError


class Bash(Shell):
    """
    The Bourne Again Shell.
    """

    def eval_completion(self, command: str = __project__) -> str:
        return f"_{command.replace('-', '_').upper()}_COMPLETE=source_bash {command}"

    def append_eval_completion(
        self, file: Path, command: str = __project__
    ) -> Path:
        eval_string = f'eval "$({self.eval_completion(command)})"'
        with file.open("a") as opened:
            opened.writelines([eval_string, ""])
        return file


class Zsh(Shell):
    """
    The Z Shell.
    """

    def eval_completion(self, command: str = __project__) -> str:
        return f"_{command.replace('-', '_').upper()}_COMPLETE=source_zsh {command}"

    def append_eval_completion(
        self, file: Path, command: str = __project__
    ) -> Path:
        eval_string = f'eval "$({self.eval_completion(command)})"'
        with file.open("a") as opened:
            opened.writelines([eval_string, ""])
        return file


def guess_shell() -> Shell:
    if "BASH_VERSION" in os.environ:
        return Bash()
    elif "ZSH_VERSION" in os.environ:
        return Zsh()
    else:
        return Shell()
