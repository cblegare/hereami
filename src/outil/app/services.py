"""
##################
Service definition
##################

This module defines factories for all internally used services.

It is in this file that implementations are **chosen**
(perhaps based on configuration or availability of
libraries or capabilities) and **wired** together.

Functions defined in this module define **available services**,
except for :func:`outil.app.services.all_definitions` which is
responsible for listing these services.  This list can be used
by *inversion of control* implementations.

This module obeys a few rules:

**Heart of instability**

    This module is intended to be highly unstable so other parts
    on the system can be more stable.

    Except for the application bootstrapping process that may use
    :func:`outil.app.services.all_definitions`, **nothing should
    depends** on symbols defined in this module.

    On the other hands, this module may import anything defined
    within the |outil| code base.

**Lazy imports**

    In order to help the **speed up** the bootstrapping sequence
    and to help work around the few remaining possibilities of
    **cyclic imports**, imports requires to create a service should
    be made **within the factory function**.

**Public is public**

    All functions defined in this module (except for
    :func:`outil.app.services.all_definitions`) are considered
    service factories and are available to the whole application
    **unless**

    *   the function is not public (its name starts with an ``_``).

**Services use services**

    A service factory function defined in this module *should*
    use other service definitions as parameters.  Parameter names
    are mapped to service names.
    See :mod:`outil.app.ioc` for details.
"""

import inspect
import sys
from pathlib import Path
from typing import Any, Callable, Dict, List, Type

from outil.app.command import Command
from outil.input import ChoicePrompt
from outil.io import SupportsString
from outil.template import Templates
from outil.workspace import Workspace


def templates() -> Templates:
    """
    Repository for all templates known in this run.
    """
    from outil.template import CautiousTemplate, Template
    from outil.template.cookiecutter import Cookiecutter
    from outil.template.setuptools import (
        DOTFILES_ENTRYPOINT_GROUP,
        SetuptoolsEntrypointTemplateRepository,
    )

    def _template_from_path(path: Path) -> Template:
        return CautiousTemplate(Cookiecutter(path))

    _templates = SetuptoolsEntrypointTemplateRepository(
        DOTFILES_ENTRYPOINT_GROUP, _template_from_path
    )

    return _templates


def cli_choose_path_by_filename() -> ChoicePrompt[SupportsString]:
    from outil.input.click import ClickChoice

    def path_filename(path: Path) -> str:
        return path.name

    return ClickChoice("Which one?", string_transform=path_filename)


def workspace(cli_choose_path_by_filename: ChoicePrompt[Path]) -> Workspace:
    from outil.workspace.project import GitRepositories

    return GitRepositories(Path.home(), cli_choose_path_by_filename)


def all_definitions() -> Dict[str, Callable[[Any], Any]]:
    """
    Provide all available service factories.
    """
    excluded = ["all_definitions", "all_commands"]
    return {
        name: function
        for name, function in inspect.getmembers(
            sys.modules[__name__], inspect.isfunction
        )
        if not name.startswith("_") and name not in excluded
    }


def all_commands() -> List[Type[Command]]:
    """
    Provide all available command classes.

    An alternative would have been to create a global container
    and register each commands with a decorator.  The
    :class:`outil.app.command.Command` do register all subclasses,
    but these subclasses needs to be imported first.

    Returns:
        All implemented (and registered here) commands.
    """
    from outil.template import Dotfiles, New  # noqa: F401

    return Command.__commands__
