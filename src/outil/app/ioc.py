"""
####################
Inversion of control
####################

This module provides dependency injection and service location
implementation for wiring together components of the application.


Using this module
=================

The proper use of the classes provided in this module is by a
top level entry point to the implementation.  For instance,

**Command line entry point**

    A command line entry point definition and argument parsing
    will need to define a command handler and associated services.

**Test runners**

    For testing purposes, an alternative way of creating the
    command handler could be implemented, possibly allowing for
    ad hoc replacement of service with stubs or spies.

Example implementation
----------------------

As a reference, is an example function that creates the command
handler provisioned with all registered services.

.. code-block:: python

    from outil.app.services import all_definitions
    from outil.app.ioc import (
        DependencyResolver, CommandHandler
    )

    def make_commandhandler() -> CommandHandler:
        builder = DependencyResolver()

        for service_name, service_factory in all_definitions().items():
            builder.register(service_factory, name=service_name)

        handler = CommandHandler(builder.build())
        return handler
"""

import inspect
import logging
from functools import partial
from typing import Any, Callable, Dict, Optional, Sequence, TypeVar

from lazy_object_proxy import Proxy  # type: ignore

from outil.app.command import CommandHandler
from outil.toposort import (
    DependencyGraph,
    flat_topological_sort,
    topological_sort,
)

log = logging.getLogger(__name__)

_Factory = TypeVar("_Factory", bound=Callable[..., Any])


ReturnType = TypeVar("ReturnType")


class ServiceMapHandler(CommandHandler):
    """
    Locate dependencies of commands and invoke them.

    This class implements a service locator to be used exclusively
    by :class:`outil.app.command.Command` objects.

    For injecting dependencies in arbitrary services,
    register them with
    :meth:`outil.app.ioc.DependencyResolver.register`.

    .. warning:: This implementation is not type checked.
        You may test for types of resolved services by looking
        for all :class:`outil.app.command.Command` classes and
        checking that all parameters specified for the
        :meth:`outil.app.command.Command.execute` matches in name
        and type a service defined in :mod:`outil.app.services`.
    """

    def __init__(self, services_map: Dict[str, Any]):
        """
        Create a command handler using pre-built services.

        Args:
            services_map: A mapping of dependency name to implementing
                instance. The resolved services built by the
                :meth:`outil.app.ioc.DependencyResolver.build`
                method is suitable for this parameter.
        """
        self._services = services_map

    def inject(
        self, callback: Callable[..., ReturnType]
    ) -> Callable[[], ReturnType]:
        """
        Locate required services and returned a parametrized function.

        Args:
            callback: A function that takes services as parameters.

        Returns:
            The same function but with services already bound as arguments.
        """
        signature = inspect.signature(callback)
        arguments = {
            name: self._services[name] for name in signature.parameters
        }

        bound = partial(callback, **arguments)
        return bound


class DependencyResolver:
    """
    Build a service container for wiring together implementations.

    This class provides a convenient way to resolve implementations
    and their code dependencies.  In other words, it is a depencency
    injection implementation.

    .. tip:: This is not a service locator
        This class works by mapping all implementation to their
        respective dependencies statically as a bulk.
        It does not provide a way to resolve a dynamic service set at
        runtime.

    This dependency injection implementation is tuned for
    **short-lived processes**, such as command line tools.
    Even if all possible services are mapped and resolved as a batch,
    ideally early during the application startup phase,
    concrete implementations will only be instanciated if and when used,
    thanks to the :mod:`lazy-object-proxy: module.

    The resulting *service container* is a simple dictionary mapping
    service names to service instances.

    .. tip:: Cycles will be detected.
        This class checks for dependency cycle each time a new service
        is registered.  As long as your service dependency graph is
        acyclic, everything should be fine.

    To use this class, proceeds in three stages:

    *   Register all you services with the :meth:`~register` method.

    *   Build the resolved services with the :meth:`~build` method.

    *   Use the built services.

    .. warning:: Using this dependency injection somewhat breaks any
        type checking you might rely on.  Indeed, it associates
        function return values to function paramter names in a way
        that no type checker will be able to track.

    Example:

        >>> resolver = DependencyResolver()
        >>> def database(connection_string):
        ...     # initialize the database with
        ...     # the provided connection string
        ...     return f"connected to {connection_string}"
        >>> _ = resolver.register(database)
        >>> _ = resolver.register(lambda: "sqlite://", "connection_string")
        >>> services_map = resolver.build()
        >>> services_map.get("database") == "connected to sqlite://"
        True
        >>> services_map.get("connection_string") == "sqlite://"
        True
    """

    def __init__(self) -> None:
        self._service_dependencies: DependencyGraph = {}
        self._service_factories: Dict[str, Callable[..., Any]] = {}
        self._cached_services: Dict[str, Any] = {}

    def build(self) -> Dict[str, Any]:
        """
        Build the resolved services along with their dependencies.

        .. important:: If called multiple times, new service instances
            are created each time.

        Return:
            A dictionary mapping service names to service (lazy)
                instances.
        """
        services_map: Dict[str, Any] = {}

        for service_name in flat_topological_sort(self._service_dependencies):
            if service_name in services_map:
                continue
            factory = self._service_factories[service_name]
            dependencies = {
                name: services_map[name]
                for name in self._service_dependencies[service_name]
            }
            bound_constructor = partial(factory, **dependencies)
            services_map[service_name] = Proxy(bound_constructor)

        return services_map

    def register(
        self,
        func: _Factory,
        name: Optional[str] = None,
        needs: Optional[Sequence[str]] = None,
        provides: Optional[Sequence[str]] = None,
    ) -> _Factory:
        """
        Register a service factory to this dependency resolver.

        This adds nodes and vertices to the internal dependency graph.
        Each registered function adds a node in the graph.
        The function's parameters add vertices to services with the same
        name as the parameter.

        Args:
            func:
                A function that would return a service when called with the
                proper arguments.
            name:
                If provided, this name is used instead of the function's
                name. This parameter must be provided when registering
                anonymous functions.
            needs:
                If provided, this maps to other nodes name to inject into
                this function arguments upon call.  If not provided, the
                function's parameter names are used.
            provides:
                For a function returning a tuple, this will create dependant
                nodes that provides each item from the tuple.

        Raise:
            ValueError:
                Circular dependency detected.
            ValueError:
                Lambda provided without an explicit name.
            ValueError:
                Name or provided name already exists.
            RuntimeError:
                When using ``provides`` and the factory's return value was
                not a sequence of enough values.

        Return:
            The factory is returned untouched.
        """
        name = name or func.__name__
        provides = provides or []
        needs = needs or [
            need for need in inspect.signature(func).parameters.keys()
        ]
        actual_provides = set(provides)
        actual_needs = set(needs)

        if any(provide == "<lambda>" for provide in actual_provides):
            raise ValueError(
                "You must provide a name when registering lambda factories"
            )

        self._service_dependencies[name] = actual_needs

        if not self._check():
            raise ValueError(
                f"Circular dependency detected while registering {name} -> {actual_needs}"
            )

        self._service_factories[name] = func

        i = 0
        for provide in actual_provides:

            def nth_of_func_returned(returned: Sequence[Any]) -> Any:
                nth = int(i)
                try:
                    value = returned[nth]
                    return value
                except IndexError as e:
                    raise RuntimeError(
                        f"Provider {name} was expected to provide "
                        f"{len(actual_provides)} values "
                        f"but at most {nth} were provided."
                    ) from e
                except TypeError as e:
                    raise RuntimeError(
                        f"Provider {name} was expected to provide "
                        f"a sequence of {len(actual_provides)} values "
                        f"but a non subscriptable object was returned."
                    ) from e

            _ = self.register(nth_of_func_returned, provide, needs=[name])
            i += 1

        return func

    def _check(self) -> bool:
        try:
            _ = topological_sort(self._service_dependencies)
        except ValueError:
            return False
        else:
            return True
