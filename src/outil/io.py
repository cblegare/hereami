"""
######################################################
A collection of filesystem or string-related utilities
######################################################
"""
from __future__ import annotations

import collections
import difflib
import filecmp
import hashlib
import io
import re
from abc import abstractmethod
from pathlib import Path
from typing import (
    TYPE_CHECKING,
    Any,
    BinaryIO,
    Dict,
    Iterator,
    Protocol,
    Sequence,
    Union,
    cast,
    overload,
    runtime_checkable,
)

from pydantic import BaseModel as _BaseModel
from ruamel.yaml import YAML
from ruamel.yaml.compat import StringIO

# Here the dircmp class is defined as a generic in the stubs
# but not generic in the implementation.
# Hence, at runtime it raises
#   TypeError: 'type' object is not subscriptable
# This is fixed in Python 3.9
# when using the annotations __future__ import.
if TYPE_CHECKING:
    DirCmp = filecmp.dircmp[str]
else:
    DirCmp = filecmp.dircmp


@runtime_checkable
class SupportsString(Protocol):
    """An ABC with one abstract method __str__."""

    @abstractmethod
    def __str__(self) -> str:
        pass


class _JSONArray(Protocol):
    def __getitem__(self, idx: int) -> "JSONLike":
        ...

    # hack to enforce an actual list
    def sort(self) -> None:
        ...


class _JSONDict(Protocol):
    def __getitem__(self, key: str) -> "JSONLike":
        ...

    # hack to enforce an actual dict
    @staticmethod
    @overload
    def fromkeys(seq: Sequence[Any]) -> Dict[Any, Any]:
        ...

    @staticmethod
    @overload
    def fromkeys(seq: Sequence[Any], value: Any) -> Dict[Any, Any]:
        ...


JSONLike = Union[str, int, float, bool, None, _JSONArray, _JSONDict]


class _MyYAML:
    def __init__(self) -> None:
        # Only use primitives (safe)
        # The C implementation misbehave with indentation, use pure python (pure)
        self._impl = YAML(typ="safe", pure=True)
        # Preserve order, otherwise it is sort()ed
        self._impl.sort_base_mapping_type_on_output = False  # type: ignore
        # Indent 2 character, with sequences indented also
        self._impl.indent(mapping=2, sequence=4, offset=2)
        # Prevent inline mappings and sequences with JSON-like representation
        self._impl.default_flow_style = False

    def dumps(self, data: Any) -> str:
        stream = StringIO()
        self._impl.dump(data, stream=stream)
        return stream.getvalue()

    def load(self, stream: Union[Path, Any]) -> JSONLike:
        """
        A safe YAML loader.
        """
        return cast(self._impl.load(stream), JSONLike)  # type: ignore


yaml = _MyYAML()


class FolderComparison:
    def __init__(self, left: Path, right: Path):
        """
        Compare folders and report differences.

        This class considers the left operand to be the "original" version
        and the right operand the "modified" version.

        Args:
            left: Reference folder
            right: Possibly modified folder
        """
        self.left = left
        self.right = right
        self._added = False
        self._removed = False
        self._changed = False
        self.reportlines = [
            line
            for comparison in self._gen_comparison()
            for line in self._gen_report_lines(comparison)
        ]

    @property
    def some_added(self) -> bool:
        """Indicate whether or not some new files where found."""
        return self._added

    @property
    def some_removed(self) -> bool:
        """Indicate whether or not some files where missing."""
        return self._removed

    @property
    def some_changed(self) -> bool:
        """Indicate whether or not some files where found but modified."""
        return self._changed

    @property
    def destructive(self) -> bool:
        """Indicate whether some files where missing or modified."""
        return self.some_changed or self.some_removed

    def _gen_comparison(self) -> Iterator[DirCmp]:
        comparisons = collections.deque(
            [filecmp.dircmp(str(self.left), str(self.right))]
        )
        while comparisons:
            comparison = comparisons.popleft()
            yield comparison
            comparisons.extend(comparison.subdirs.values())

    def _gen_report_lines(self, comparison: DirCmp) -> Iterator[str]:
        for added in comparison.right_only:
            self._added = True
            yield f"{added} was added"
        for removed in comparison.left_only:
            self._removed = True
            yield f"{removed} was removed"
        for different in comparison.diff_files:
            self._changed = True
            left_file = Path(comparison.left, different)
            right_file = Path(comparison.right, different)

            abstract_left_path = Path(
                "<before>", left_file.relative_to(self.left.parent)
            )
            abstract_right_path = Path(
                "<after>", right_file.relative_to(self.right.parent)
            )

            yield from difflib.unified_diff(
                left_file.open().readlines(),
                right_file.open().readlines(),
                fromfile=str(abstract_left_path),
                tofile=str(abstract_right_path),
            )


def snake_case(string: str) -> str:
    """
    Convert a string to ``snake_case``

    .. warning:: The algorithm used here is destructive.  That means
        that the reconstruction of specific samples to, for instance,
        ``camelCase``, would be impossible or at least very difficult
        and involving a database of known acronyms or the like.

    Examples:

        >>> snake_case("aA")
        'a_a'
        >>> snake_case("aAa")
        'a_aa'
        >>> snake_case("snake2_snake")
        'snake_2_snake'
        >>> snake_case("get200HTTPResponseCode")
        'get_200_http_response_code'
        >>> snake_case("10CoolDudes")
        '10_cool_dudes'
        >>> snake_case("Camel2WARNING_Case_CASE")
        'camel_2_warning_case_case'

    Args:
        string: Some string in any case.

    Returns:
        A nice and neat camel case string.
    """
    pattern = re.compile(
        r"[A-Z]?[a-z]+|[A-Z]{2,}(?=[A-Z][a-z]|\d|\W|$)|\d+|[A-Z]{2,}|[A-Z]$"
    )
    words = pattern.findall(string)
    return "_".join(map(str.lower, words))


def hash_for_file(
    fileobj: BinaryIO,
    algorithm: str = "sha1",
    block_size: int = io.DEFAULT_BUFFER_SIZE,
) -> hashlib._Hash:
    """
    Compute the checksum of a file object.

    Args:
        fileobj: File-like object.
        algorithm: Hashing algorithm to use.
        block_size: Read the file by chunks of this size.
    """
    if algorithm not in hashlib.algorithms_available:
        raise NameError(
            '"{algorithm}" is not available.'.format(algorithm=algorithm)
        )

    # According to hashlib documentation using new()
    # will be slower than calling using named
    # constructors, ex.: hashlib.md5()
    hasher = hashlib.new(algorithm)

    for chunk in iter(lambda: fileobj.read(block_size), b""):
        hasher.update(chunk)

    return hasher


def same_files(left: Path, right: Path) -> bool:
    """
    Check whether two files have the same content.

    Identical files having different *names* or *filesystem attributes*
    will return ``True``.

    .. attention:: This comparison involve reading the entire files content.
        The implementation should not leak memory but could take time with
        large files.

    Args:
        left: Left operand, some file
        right: Right operand, some other file

    Returns:

    """
    with left.open("rb") as left_opened:
        left_sum = hash_for_file(left_opened).hexdigest()
    with right.open("rb") as right_opened:
        right_sum = hash_for_file(right_opened).hexdigest()

    return left_sum == right_sum


class BaseModel(_BaseModel):
    def yaml(self, *args, with_root_key: bool = False, **kwargs) -> str:
        if with_root_key:
            root_key = snake_case(self.__class__.__name__)
            data = {root_key: {}}
            data[root_key].update(yaml.load(self.json(*args, **kwargs)))
        else:
            data = yaml.load(self.json(*args, **kwargs))

        return yaml.dumps(data)
