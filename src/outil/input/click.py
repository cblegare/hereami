from __future__ import annotations

from typing import Callable, Optional, Sequence, TypeVar

import click

from outil.io import SupportsString

_T = TypeVar("_T", bound=SupportsString)


class ClickChoice:
    def __init__(
        self, text: str, string_transform: Optional[Callable[[_T], str]] = None
    ):
        self._text = text
        self._transform = string_transform or str

    def __call__(self, choices: Sequence[_T]) -> Optional[_T]:
        choice_map = {self._transform(choice): choice for choice in choices}
        choice = click.prompt(
            self._text, type=click.Choice([choice for choice in choice_map])
        )

        try:
            return choice_map[choice]
        except KeyError:
            return None
