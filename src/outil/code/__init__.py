import logging
from typing import Any, Dict

log = logging.getLogger(__name__)


def known_howtos() -> Dict[str, Any]:
    return {"format": format}


def do(*things: str) -> int:
    log.critical(things)
    return 0


def format() -> None:
    """
    How to format my code?



    Returns:

    """
