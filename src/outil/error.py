from __future__ import annotations


class ToolError(RuntimeError):
    pass


class SubprocessFailed(ToolError):
    def __init__(self, returncode: int):
        self.returncode = returncode
        super().__init__(returncode)


class DependencyMissing(ToolError):
    def __init__(self, package_name: str):
        super().__init__(f"You need to install '{package_name}'.")
